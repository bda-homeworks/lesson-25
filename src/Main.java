import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) throws SQLException, ParseException {
        String url = "jdbc:mysql://localhost:3363/lesson25";
        String name = "root";
        String password = "windows8";
        Scanner input = new Scanner(System.in);

        while (true) {
            System.out.println("1 - Insert Student\n2 - Insert Teacher\n3 - Update Student\n4 - Update Teacher\n5 - Show Students\n6 - Show Teachers\n7 - Exit.");
            System.out.println("-------------------------------------------");
            System.out.print("Choice: ");
            int choice = input.nextInt();
            switch (choice) {
                case 1:
                    studentInsert(input, url, name, password);
                    break;
                case 2:
                    teacherInsert(input, url, name, password);
                    break;
                case 3:
                    updateStudent(input, url, name, password);
                case 5:
                    showStudent(url, name, password);
                    break;
                case 6:
                    showTeacher(url, name, password);
                case 7:
                    System.exit(0);
            }
        }
    }

    public static void studentInsert(Scanner input, String url, String name, String password) throws ParseException,SQLException {
        System.out.println("-------------------------------------------");
        System.out.println("Insert into student >");
        System.out.print("1- Name: ");
        String nameInsert = input.next();
        System.out.print("2- Surname: ");
        String surnameInsert = input.next();
        System.out.print("3- Fee: ");
        int feeInsert = input.nextInt();
        System.out.print("4- Start date (yyyy-MM-dd): ");
        String startDateInsert = input.next();
        System.out.print("5- Leave date (yyyy-MM-dd): ");
        String leaveDateInsert = input.next();
        System.out.print("6- Teacher ID: ");
        int teacherIdInsert = input.nextInt();
        System.out.print("7- Student Row ID: ");
        int studentRowIdInsert = input.nextInt();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date utilDate = dateFormat.parse(startDateInsert);
        java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

        java.util.Date utilDateLeave = dateFormat.parse(leaveDateInsert);
        java.sql.Date sqlDateLeave = new java.sql.Date(utilDateLeave.getTime());


        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        try {
            Connection con = DriverManager.getConnection(url, name, password);
            String insertQuery = "insert into student (name, surname, fee, start_date, leave_date, teacher_id, student_row_info_id) VALUES (?,?,?,?,?,?,?)";
            PreparedStatement prepStat = con.prepareStatement(insertQuery);
            prepStat.setString(1, nameInsert);
            prepStat.setString(2, surnameInsert);
            prepStat.setInt(3,feeInsert);
            prepStat.setString(4, String.valueOf(sqlDate));
            prepStat.setString(5, String.valueOf(sqlDateLeave));
            prepStat.setInt(6,teacherIdInsert);
            prepStat.setInt(7, studentRowIdInsert);
            prepStat.executeUpdate();

            if (nameInsert != null) {
                String secondQuery = "insert into student_row_info (create_date, update_date, status) VALUES (NOW(),NOW(),?)";
                PreparedStatement prepStatSecond = con.prepareStatement(secondQuery);
                prepStatSecond.setInt(1, '1');
                prepStatSecond.executeUpdate();
                prepStatSecond.close();
            }

            prepStat.close();
            con.close();
            System.out.println("Student " + nameInsert + " successfully inserted!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public static void teacherInsert(Scanner input, String url, String name, String password) throws ParseException,SQLException {
        System.out.println("-------------------------------------------");
        System.out.println("Insert into teacher >");
        System.out.print("1- Name: ");
        String nameInsert = input.next();
        System.out.print("2- Surname: ");
        String surnameInsert = input.next();
        System.out.print("3- Salary: ");
        int feeInsert = input.nextInt();
        System.out.print("4- Start date (yyyy-MM-dd): ");
        String startDateInsert = input.next();
        System.out.print("5- Leave date (yyyy-MM-dd): ");
        String leaveDateInsert = input.next();
        System.out.print("6- Teacher Row ID: ");
        int teacherIdInsert = input.nextInt();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date utilDate = dateFormat.parse(startDateInsert);
        java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

        java.util.Date utilDateLeave = dateFormat.parse(leaveDateInsert);
        java.sql.Date sqlDateLeave = new java.sql.Date(utilDateLeave.getTime());


        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        try {
            Connection con = DriverManager.getConnection(url, name, password);
            String insertQuery = "insert into teacher (name, surname, salary, start_date, leave_date, teacher_row_info_id) VALUES (?,?,?,?,?,?)";
            PreparedStatement prepStat = con.prepareStatement(insertQuery);
            prepStat.setString(1, nameInsert);
            prepStat.setString(2, surnameInsert);
            prepStat.setInt(3,feeInsert);
            prepStat.setString(4, String.valueOf(sqlDate));
            prepStat.setString(5, String.valueOf(sqlDateLeave));
            prepStat.setInt(6,teacherIdInsert);
            prepStat.executeUpdate();

            if (nameInsert != null) {
                String secondQuery = "insert into teacher_row_info (create_date, update_date, status) VALUES (NOW(),NOW(),?)";
                PreparedStatement prepStatSecond = con.prepareStatement(secondQuery);
                prepStatSecond.setInt(1, '1');
                prepStatSecond.executeUpdate();
                prepStatSecond.close();
            }

            prepStat.close();
            con.close();
            System.out.println("Teacher " + nameInsert + " successfully inserted!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public static void showStudent (String url, String name, String password) throws SQLException {
        Connection con = DriverManager.getConnection(url, name, password);
        Statement statement = con.createStatement();
        String query = "Select * from student";
        ResultSet resultSet = statement.executeQuery(query);

        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String nameStu = resultSet.getString("name");
            String surnameStu = resultSet.getString("surname");
            int fee = resultSet.getInt("fee");
            String createDate = resultSet.getString("start_date");
            String leaveDate = resultSet.getString("leave_date");
            int teacherId = resultSet.getInt("teacher_id");
            int rowId = resultSet.getInt("student_row_info_id");
            System.out.println(id + " " + nameStu + " " + surnameStu + " " + createDate + " " + leaveDate + " " + fee + " " + teacherId + " " + rowId);
            System.out.println("-------------------------------------------");
        }

    }
    public static void showTeacher (String url, String name, String password) throws SQLException {
        Connection con = DriverManager.getConnection(url, name, password);
        Statement statement = con.createStatement();
        String query = "Select * from teacher";
        ResultSet resultSet = statement.executeQuery(query);

        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String nameStu = resultSet.getString("name");
            String surnameStu = resultSet.getString("surname");
            int fee = resultSet.getInt("salary");
            String createDate = resultSet.getString("start_date");
            String leaveDate = resultSet.getString("leave_date");
            int rowId = resultSet.getInt("teacher_row_info_id");
            System.out.println(id + " " + nameStu + " " + surnameStu + " " + createDate + " " + leaveDate + " " + fee + " " + " " + rowId);
            System.out.println("-------------------------------------------");
        }
    }

    public static void updateStudent (Scanner input, String url, String name, String password) throws SQLException {
        Connection connection = DriverManager.getConnection(url, name, password);
        String updateQuery = "UPDATE student SET student.name = ?, student.surname = ?,student.fee = ?,student.teacher_id = ? where id = ?";
        PreparedStatement preparedStatementUpdate = connection.prepareStatement(updateQuery);

        System.out.println("-------------------------------------------");
        System.out.print("Enter the ID of student that you want to change: ");
        int searchId = input.nextInt();
        preparedStatementUpdate.setInt(5,searchId);
        System.out.println("1 - Change name\n2 - Change surname\n3 - Change fee\n4 - Change teacher ID\n5 - Change everything");
        int choice = input.nextInt();
        switch (choice) {
            case 1:
                System.out.print("Enter the new Name for student: ");
                String updateName = input.next();
                preparedStatementUpdate.setString(1, updateName);
                preparedStatementUpdate.close();
                connection.close();
                break;
            case 2:
                System.out.print("Enter the new Surname for student: ");
                String updateSurname = input.next();
                preparedStatementUpdate.setString(2, updateSurname);
                preparedStatementUpdate.executeUpdate();
                preparedStatementUpdate.close();
                connection.close();
                break;
            case 3:
                System.out.print("Change the Teacher ID for student: ");
                int updateTeacher = input.nextInt();
                preparedStatementUpdate.setInt(4, updateTeacher);
                preparedStatementUpdate.close();
                connection.close();
                break;
            case 4:
                System.out.print("Change the Fee for student: ");
                int updateFee = input.nextInt();
                preparedStatementUpdate.setInt(3, updateFee);
                preparedStatementUpdate.close();
                connection.close();
                break;
            case 5:
                System.out.print("Enter the new Name for student: ");
                String updateName1 = input.next();
                System.out.print("Enter the new Surname for student: ");
                String updateSurname1 = input.next();
                System.out.print("Change the Teacher ID for student: ");
                int updateTeacher1 = input.nextInt();
                System.out.print("Enter the new Fee for student: ");
                int updateFee1 = input.nextInt();

                preparedStatementUpdate.setString(1, updateName1);
                preparedStatementUpdate.setString(2, updateSurname1);
                preparedStatementUpdate.setInt(3, updateFee1);
                preparedStatementUpdate.setInt(4, updateTeacher1);
                preparedStatementUpdate.close();
                connection.close();
                break;
            default:
                System.out.println("No such choice.....");
                break;
        }
    }
}
